#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_PATH := $(call my-dir)

################################################################
include $(CLEAR_VARS)

LOCAL_MODULE := eSpeakNG
LOCAL_MODULE_CLASS := APPS
LOCAL_PRODUCT_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_DEX_PREOPT := false

LOCAL_OVERRIDES_PACKAGES := PicoTTS

LOCAL_SRC_FILES := packages/$(LOCAL_MODULE).apk

include $(BUILD_PREBUILT)
################################################################
#Credit: https://github.com/LineageOS/android_packages_apps_Etar/commit/58097aa31ab16b968603b1567a70daec45bdf743
include $(CLEAR_VARS)
LOCAL_MODULE := ws.xsoh.etar.allowlist.xml
LOCAL_MODULE_CLASS := ETC
LOCAL_PRODUCT_MODULE := true
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_PATH := $(TARGET_OUT_ETC)/sysconfig
LOCAL_SRC_FILES := $(LOCAL_MODULE)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)

LOCAL_MODULE := EtarPrebuilt
LOCAL_MODULE_CLASS := APPS
LOCAL_PRODUCT_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_DEX_PREOPT := false

LOCAL_OVERRIDES_PACKAGES := Calendar Etar
-LOCAL_REQUIRED_MODULES := ws.xsoh.etar.allowlist.xml

LOCAL_SRC_FILES := packages/$(LOCAL_MODULE).apk

include $(BUILD_PREBUILT)
################################################################
include $(CLEAR_VARS)

LOCAL_MODULE := F-DroidOfficial
LOCAL_MODULE_CLASS := APPS
LOCAL_PRODUCT_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_DEX_PREOPT := false

LOCAL_SRC_FILES := packages/$(LOCAL_MODULE).apk

LOCAL_OPTIONAL_USES_LIBRARIES := androidx.window.extensions androidx.window.sidecar

include $(BUILD_PREBUILT)
################################################################
include $(CLEAR_VARS)

LOCAL_MODULE := OpenCamera
LOCAL_MODULE_CLASS := APPS
LOCAL_PRODUCT_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_DEX_PREOPT := false

LOCAL_OVERRIDES_PACKAGES := Aperture Camera Camera2 LegacyCamera Snap

LOCAL_SRC_FILES := packages/$(LOCAL_MODULE).apk

include $(BUILD_PREBUILT)
################################################################
include $(CLEAR_VARS)

LOCAL_MODULE := SimpleGallery
LOCAL_MODULE_CLASS := APPS
LOCAL_PRODUCT_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_DEX_PREOPT := false

LOCAL_OVERRIDES_PACKAGES := Gallery2

LOCAL_SRC_FILES := packages/$(LOCAL_MODULE).apk

include $(BUILD_PREBUILT)
################################################################
include $(CLEAR_VARS)

LOCAL_MODULE := TalkBack
LOCAL_MODULE_CLASS := APPS
LOCAL_PRODUCT_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_DEX_PREOPT := false

LOCAL_SRC_FILES := packages/$(LOCAL_MODULE).apk

include $(BUILD_PREBUILT)
################################################################
include $(CLEAR_VARS)

LOCAL_MODULE := TalkBackLegacy
LOCAL_MODULE_CLASS := APPS
LOCAL_PRODUCT_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_DEX_PREOPT := false

LOCAL_SRC_FILES := packages/$(LOCAL_MODULE).apk

include $(BUILD_PREBUILT)
################################################################
